% -------------------------------------------------------------------------
function hist = get_hist(vocab, im)
% -------------------------------------------------------------------------

kdtree = vl_kdtreebuild(vocab) ;
numSpatial=[2,4]; 
im = standarizeImage(im) ;
width = size(im,2) ;
height = size(im,1) ;
numWords = size(vocab, 2) ;

% get PHOW features
[frames, descrs] = vl_phow(im, 'Verbose', 2, 'Sizes', 7, 'Step', 5) ;

% quantize local descriptors into visual words

    binsa = double(vl_kdtreequery(kdtree, vocab, ...
                                  single(descrs), ...
                                  'MaxComparisons', 50)) ;


for i = 1:length(numSpatial)
  binsx = vl_binsearch(linspace(1,width,numSpatial(i)+1), frames(1,:)) ;
  binsy = vl_binsearch(linspace(1,height,numSpatial(i)+1), frames(2,:)) ;

  % combined quantization
  bins = sub2ind([numSpatial(i), numSpatial(i), numWords], ...
                 binsy,binsx,binsa) ;
  hist = zeros(numSpatial(i) * numSpatial(i) * numWords, 1) ;
  hist = vl_binsum(hist, ones(size(bins)), bins) ;
  hists{i} = single(hist / sum(hist)) ;
end
hist = cat(1,hists{:}) ;
hist = hist / sum(hist) ;% -------------------------------------------------------------------------