input('Please change the names of the classes to the classes to be classified. The names of the classes must be same as folder name in which images are stored');

num_train =input('Please enter number of training images ') ;
num_testing =input('Please Enter number of testing images ') ; 
numb_Words =600 ;
descrs = {} ;

biasMultiplier=1 ;
num_Spatial=[2,4]; 
quantizer = 'kdtree' ;
phow_options= {'Verbose', 2, 'Sizes', 7, 'Step', 5} ;

%%Please change the names of the classes to the classes to be classified. The names of the classes must be same as folder name in which images are stored 
classes={'practical car','cartoon car','sketch car','toy car'}; 
%% Please do it

images = {} ;     
imageClass = {} ;

d = input('Please Enter the parent directory of stored data, Leave empty if directory is in workspace ','s'); 
data_dir= d; %%'101_ObjectCategories' ;


for ci = 1:length(classes)
  ims = dir(fullfile(data_dir, classes{ci}, '*.jpg'))' ;
  ims = vl_colsubset(ims, num_train +num_testing) ;
  ims = cellfun(@(x)fullfile(classes{ci},x),{ims.name},'UniformOutput',false) ;
  images = {images{:}, ims{:}} ;
  imageClass{end+1} = ci * ones(1,length(ims)) ;
end

selTrain = find(mod(0:length(images)-1,num_train+num_testing) < num_train) ;
selTest = setdiff(1:length(images), selTrain) ;
imageClass = cat(2, imageClass{:}) ;


%%for making of vocabulary
selTrainFeats = vl_colsubset(selTrain, 40) ;
  descrs = {} ;
  
now= 1 ; 
while(now<=length(selTrainFeats))

im = imread(fullfile(data_dir, images{selTrainFeats(now)})) ;
im = standarizeImage(im) ;
[drop, descrs{now}] = vl_phow(im, phow_options{:}) ;
now=now+1 ;
end

descrs = vl_colsubset(cat(2, descrs{:}), 10e4) ;
descrs = single(descrs) ;

%%quantizer
vocab = vl_kmeans(descrs, numb_Words, 'verbose', 'algorithm', 'elkan', 'MaxNumIterations', 50) ;

kdtree = vl_kdtreebuild(vocab) ;





%%computing Spatial Histograms
now=1 ; 
hists={} ;
while(now<=length(images))
image = imread(fullfile(data_dir, images{now})) ;
now=now+1 ;
image = standarizeImage(image) ;
width = size(image,2) ;
height = size(image,1) ;
numWords = size(vocab, 2) ;

% get PHOW features
[frames, descrs] = vl_phow(image,phow_options{:}) ;

% quantize local descriptors into visual words
    binsa = double(vl_kdtreequery(kdtree, vocab,single(descrs), 'MaxComparisons', 50)) ;


    for i = 1:length(num_Spatial)
      binsx = vl_binsearch(linspace(1,width,num_Spatial(i)+1), frames(1,:)) ;
      binsy = vl_binsearch(linspace(1,height,num_Spatial(i)+1), frames(2,:)) ;

      % combined quantization
      bins = sub2ind([num_Spatial(i),num_Spatial(i), numb_Words],binsy,binsx,binsa) ;
      hist = zeros(num_Spatial(i) * num_Spatial(i) * numb_Words, 1) ;
      hist = vl_binsum(hist, ones(size(bins)), bins) ;
      hists2{i} = single(hist / sum(hist)) ;
    end
hist = cat(1,hists2{:}) ;
hist = hist / sum(hist) ;

hists{now}=  hist;
end
hists = cat(2, hists{:}) ;

%%computing feature map

psix = vl_homkermap(hists, 1, 'kchi2', 'gamma', .5) ;

%%training of SVM starts
lambda = 1 / (10 *  length(selTrain)) ;
w = [] ;
      parfor ci = 1:length(classes)
        perm = randperm(length(selTrain)) ;
        fprintf('Training model for class %s\n', classes{ci}) ;
        y = 2 * (imageClass(selTrain) == ci) - 1 ;
        [w(:,ci) b(ci) info] = vl_svmtrain(psix(:, selTrain(perm)), y(perm), lambda, ...
          'Solver', 'sdca', ...
          'MaxNumIterations', 50/lambda, ...
          'BiasMultiplier', biasMultiplier, ...
          'Epsilon', 1e-3);
      end
      
b= biasMultiplier*b; 
      
% Estimate the class of the test images
scores = w' * psix + b' * ones(1,size(psix,2)) ;
[drop, imageEstClass] = max(scores, [], 1) ;


idx = sub2ind([length(classes), length(classes)], ...
              imageClass(selTest), imageEstClass(selTest)) ;
confus = zeros(length(classes)) ;
confus = vl_binsum(confus, ones(size(idx)), idx) ;



% Plots
figure(1) ; clf;
subplot(1,2,1) ;
imagesc(scores(:,[selTrain selTest])) ; title('Scores') ;
set(gca, 'ytick', 1:length(classes), 'yticklabel', classes) ;
subplot(1,2,2) ;
imagesc(confus) ;
title(sprintf('Confusion matrix (%.2f %% accuracy)', ...
              100 * mean(diag(confus)/num_testing) )) ;
%%print('-depsc2', [conf.resultPath '.ps']) ;
%%save([conf.resultPath '.mat'], 'confus', 'conf') ;


